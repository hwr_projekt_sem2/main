Die PHP-Dateien wurden zu lokalen Testzwecken erstellt und hier eingefügt.
Für die weiteren Schritte sind diese Dateien ABSOLET.
Des Weiteren werden sie auch nicht mehr zu lokalen Testzwecken verwendet.
Man kann diese Dateien als eine API betrachten, die im Notfall nochmal verwendet werden könnte.