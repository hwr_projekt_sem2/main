<?php

class DbOperation {

    private $con;

    function __construct() {

        require_once dirname( __FILE__ ).'/DbConnect.php';

        $db = new DB_CONNECT();

        $this->con = $db->connect();
    }
    /*CRUD -> C -> CREATE*/

    function createLitter( $description, $radiant, $resolved, $latitude, $longitude ) {
        $stmt = $this->con->prepare( "INSERT INTO litters (Id, description, radiant, resolved, latitude, longitude) VALUES (NULL, ?, ?, ?, ?, ?);" );
        $stmt->bind_param( "siidd", $description, $radiant, $resolved, $latitude, $longitude );

        if ( $stmt->execute() ) {
            return true;
        } else {
            return false;
        }
        mysqli_close( $this->con );
    }

    function getAllLitter() {
        $stmt = $this->con->prepare( "SELECT Id, description, radiant, resolved, latitude, longitude FROM litters" );
        
        if ( $stmt->execute() ) {
            $stmt->bind_result($Id, $description, $radiant, $resolved, $latitude, $longitude);

            $litterArray = array();

            while($stmt->fetch()) {
                $temp = array();
                $temp['Id'] = $Id;
                $temp['description'] = $description;
                $temp['radiant'] = $radiant;
                $temp['resolved'] = $resolved;
                $temp['latitude'] = $latitude;
                $temp['longitude'] = $longitude;
                array_push($litterArray, $temp);
            }
            echo json_encode( $litterArray );
            return true;
        } else {
            return false;
        }
        mysqli_close( $this->con );
    }

    function deleteLitter( $Id ) {
        $stmt = $this->con->prepare("DELETE FROM litters WHERE Id = ?");
        $stmt->bind_param("i", $Id);
        
        if ( $stmt->execute() ) {
            return true;
        } else {
            return false;
        }
        mysqli_close( $this->con );
    }
    
    function updateLitter ( $Id, $description, $radiant, $resolved, $latitude, $longitude) {
        $stmt = $this->con->prepare("UPDATE litters SET description = ?, radiant = ?, resolved = ?, latitude = ?, longitude = ? WHERE Id = ?");
        $stmt->bind_param("siiddi", $description, $radiant, $resolved, $latitude, $longitude, $Id);
        
        if ( $stmt->execute() ) {
            return true;
        } else {
            return false;
        }
        mysqli_close( $this->con );
    }
}
?>