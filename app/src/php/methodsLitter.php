<?php

require_once __DIR__ . '/DbOperation.php';

$response = array();

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    if (
        isset( $_POST['description'] ) and
        isset( $_POST['radiant'] ) and
        isset( $_POST['resolved'] ) and
        isset( $_POST['latitude'] ) and
        isset( $_POST['longitude'] )
    ) {
        //operate the data further
        $db = new DbOperation();

        if ( $db->createLitter(
            $_POST['description'],
            $_POST['radiant'],
            $_POST['resolved'],
            $_POST['latitude'],
            $_POST['longitude'] ) ) {
                $response['success'] = 200;
                $response['message'] = "Müllstelle wurde erfolgreich registriert!";
            } else {
                $response['success'] = 400;
                $response['message'] = "Ein Fehler ist aufgetreten! Versuche es erneut!";
            }
        } else {
            $response['success'] = 404;
            $response['message'] = "Anfragefelder fehlen!";
        }
    } elseif ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
        $db = new DbOperation();

        if ( $db->getAllLitter() ) {
            $response['success'] = 200;
            $response['message'] = "Müllstellen wurden erfolgreich auf der Karte dargestellt!";
        } else {
            $response['success'] = 400;
            $response['message'] = "Ein Fehler ist aufgetreten! Versuche es erneut!";
        }
    } elseif ( $_SERVER['REQUEST_METHOD'] == 'DELETE' ) {
        if ( isset( $_GET['Id'] ) ) {
            $db = new DbOperation();

            if ( $db->deleteLitter( $_GET['Id'] ) ) {
                $response['success'] = 200;
                $response['message'] = "Müllstelle wurde erfolgreich gelöscht!";
            } else {
                $response['success'] = 400;
                $response['message'] = "Ein Fehler ist aufgetreten! Versuche es erneut!";
            }
        } else {
            $response['success'] = 404;
            $response['message'] = "Anfragefelder fehlen!";

        }
    } elseif ( $_SERVER['REQUEST_METHOD'] == 'PATCH' ) {
        if ( isset( $_GET['Id'] ) and
             isset( $_POST['description'] ) and
             isset( $_POST['radiant'] ) and
             isset( $_POST['resolved'] ) and
             isset( $_POST['latitude'] ) and
             isset( $_POST['longitude'] )
            ){
            $db = new DbOperation();

            if ( $db->updateLitter( 
                $_GET['Id'],
                $_POST['description'],
                $_POST['radiant'],
                $_POST['resolved'],
                $_POST['latitude'],
                $_POST['longitude'] ) ) {
                    $response['success'] = 200;
                    $response['message'] = "Müllstelle wurde erfolgreich aktualisiert!";
            } else {
                $response['success'] = 400;
                $response['message'] = "Ein Fehler ist aufgetreten! Versuche es erneut!";
            }
        } else {
            $response['success'] = 404;
            $response['message'] = "Anfragefelder fehlen!";
        }
    } else {
        $response['success'] = 405;
        $response['message'] = "Ungültige Anfrage!";
    }
    echo json_encode( $response, JSON_UNESCAPED_UNICODE  | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK );

    ?>
