package com.example.muelapp;

public enum RelationshipType {

    INCOMINGFRIEND(1),
    OUTGOINGFRIEND(2),
    FRIEND(3),
    BLOCKED(4);

    private Integer code;

    RelationshipType(Integer x){
        this.code = x;
    }

    public static RelationshipType fromCode(Integer code) {
        for (RelationshipType relationshipType :RelationshipType.values()){
            if (relationshipType.getCode().equals(code)){
                return relationshipType;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }
}
