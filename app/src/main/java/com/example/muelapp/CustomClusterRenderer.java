package com.example.muelapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

import java.util.Objects;

import static androidx.appcompat.content.res.AppCompatResources.getDrawable;

public class CustomClusterRenderer extends DefaultClusterRenderer<SingleMarker> {

    private final Context mContext;
    private final IconGenerator mClusterIconGenerator;

    public CustomClusterRenderer(Context context, GoogleMap map, ClusterManager<SingleMarker> clusterManager) {
        super(context, map, clusterManager);

        mContext = context;
        mClusterIconGenerator = new IconGenerator(mContext);
    }


    @Override
    protected void onBeforeClusterRendered(Cluster<SingleMarker> cluster, MarkerOptions markerOptions) {
        super.onBeforeClusterRendered(cluster, markerOptions);

        final Drawable clusterIcon = getDrawable(mContext, R.drawable.ic_baseline_cluster_form);
        if (clusterIcon != null) {
            mClusterIconGenerator.setBackground(clusterIcon);

            GetDisplayMobileSize getDisplayMobileSize = new GetDisplayMobileSize(mContext);
            int width = getDisplayMobileSize.getWidth();
            int height = getDisplayMobileSize.getHeight();
            //modify padding for one or two digit numbers
            if (width <= 800 && height <= 800) {
                if (cluster.getSize() < 10) {
                    mClusterIconGenerator.setContentPadding(35, 30, 35, 33);
                } else {
                    mClusterIconGenerator.setContentPadding(33, 32, 33, 32);
                }
            } else {
                if (cluster.getSize() < 10) {
                    mClusterIconGenerator.setContentPadding(68, 50, 68, 54);
                } else {
                    mClusterIconGenerator.setContentPadding(60, 55, 60, 59);
                }
            }
            mClusterIconGenerator.setTextAppearance(R.style.amu_Bubble_TextAppearance_Light);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }
    }


    @Override
    protected void onBeforeClusterItemRendered(SingleMarker item, MarkerOptions markerOptions) {
        super.onBeforeClusterItemRendered(item, markerOptions);

        final BitmapDescriptor markerDeskriptor = getBitmapDescriptor(R.drawable.ic_pin_inner_circle);
        markerOptions.icon(markerDeskriptor);
    }


    @SuppressLint("ObsoleteSdkInt")
    private BitmapDescriptor getBitmapDescriptor(int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            VectorDrawable vectorDrawable = (VectorDrawable) getDrawable(mContext, id);

            int h = Objects.requireNonNull(vectorDrawable).getIntrinsicHeight();
            int w = vectorDrawable.getIntrinsicWidth();

            vectorDrawable.setBounds(0, 0, w, h);

            Bitmap bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bm);
            vectorDrawable.draw(canvas);

            return BitmapDescriptorFactory.fromBitmap(bm);
        } else {
            return BitmapDescriptorFactory.fromResource(id);
        }
    }
}
