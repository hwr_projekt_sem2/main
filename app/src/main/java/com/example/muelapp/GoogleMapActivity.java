package com.example.muelapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.navigation.NavigationView;

public class GoogleMapActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    //Layout für das Side-Menü
    private DrawerLayout drawer;
    private boolean homeActive;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_maps);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.login_toolbar_neu);
        setSupportActionBar(toolbar);

        //Drawer
        drawer = findViewById(R.id.activity_google_maps);
        navigationView = findViewById(R.id.nav_view);
        setProfileAttributes(navigationView);
        navigationView.setNavigationItemSelectedListener(this);

        //Öffnen und Schließen durch Toolbar-Button (3 Striche am linken Ende)
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //Sorgt dafür, dass die App kein Neustart ausführt, beim Übergehen im horizontalen Modus des Handys
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_home);
            homeActive = true;
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        //Öffnen von Activitys (Fragments) durch Side-Menü
        switch (item.getItemId()) {
            case R.id.nav_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                homeActive = true;
                break;
            case R.id.nav_friends:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new FriendsFragment()).addToBackStack(null).commit();
                homeActive = false;
                break;
            case R.id.nav_groups:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new GroupsFragment()).addToBackStack(null).commit();
                Toast.makeText(getApplicationContext(), "Wird noch entwickelt.", Toast.LENGTH_LONG).show();
                homeActive = false;
                break;
            case R.id.nav_profile:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ProfileFragment()).addToBackStack(null).commit();
                Toast.makeText(getApplicationContext(), "Wird noch entwickelt.", Toast.LENGTH_LONG).show();
                homeActive = false;
                break;
            case R.id.nav_settings:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SettingsActivity.SettingsFragment()).addToBackStack(null).commit();
                homeActive = false;
                break;
            case R.id.nav_logout:
                Intent logout = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(logout);
                this.finish();
                homeActive = false;
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(@NonNull MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    public void onBackPressed() {
        if (!homeActive) {
            FragmentManager fm = getSupportFragmentManager();
            for (Fragment fragment : fm.getFragments()) {
                if (fragment instanceof HomeFragment) {
                    homeActive = true;
                    break;
                }
            }

            if (!homeActive) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    navigationView.setCheckedItem(R.id.nav_home);
                    super.onBackPressed();
                }
            }
        } else {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            }
        }
    }

    private void setProfileAttributes(@NonNull NavigationView navigationView) {
        View navHeaderProfile = navigationView.getHeaderView(0);
        TextView textViewUserName = navHeaderProfile.findViewById(R.id.nav_header_username);
        TextView textViewUserMail = navHeaderProfile.findViewById(R.id.nav_header_mail);
        textViewUserName.setText(CurrentUser.getName());
        textViewUserMail.setText(CurrentUser.getEmail());
    }
}