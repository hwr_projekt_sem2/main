package com.example.muelapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;


public class ProfileActivitylogFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutmanager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profilefragment_activitylog, container, false);



        //ArrayList<ActivitylistItem> activityList = GoogleMapActivity.activityList;

        //activityList.add(new ActivitylistItem(R.drawable.ic_markierung, "Markierung gesetzt", "12.10.2020"));
        //activityList.add(new ActivitylistItem(R.drawable.ic_markierung, "Markierung gesetzt", "13.10.2020"));
        //activityList.add(new ActivitylistItem(R.drawable.ic_stelleweg, "Stelle gereiningt", "14.10.2020"));

        mRecyclerView = view.findViewById(R.id.activityList);
        mRecyclerView.setHasFixedSize(true);
        mLayoutmanager = new LinearLayoutManager(getContext());
        //mAdapter = new ProfileAdapter(activityList);

        mRecyclerView.setLayoutManager(mLayoutmanager);
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }
}
