package com.example.muelapp;

import androidx.annotation.NonNull;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;

public class RequestHandler {

    public static String sendPost(String r_url , JSONObject postDataParams, @NonNull String patchOrPost) throws Exception {
        URL url = new URL(r_url);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(20000);
        conn.setConnectTimeout(20000);

        if(patchOrPost.equals("post")) {
            conn.setRequestMethod("POST");
        } else {
            conn.setRequestMethod("PATCH");
        }
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("AUTHENTICATION", CurrentUser.getToken());
        OutputStream os = conn.getOutputStream();
       // os.write(encodeParams(postDataParams).getBytes());
        BufferedWriter writer = new BufferedWriter( new OutputStreamWriter(os, StandardCharsets.UTF_8));
        writer.write(postDataParams.toString());
        writer.flush();
        writer.close();
        os.close();

        int responseCode=conn.getResponseCode(); // To Check for 200
        if (responseCode == HttpsURLConnection.HTTP_OK || responseCode == HttpsURLConnection.HTTP_CREATED) {

            BufferedReader in=new BufferedReader( new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder("");
            String line="";
            while((line = in.readLine()) != null) {
                sb.append(line);
                break;
            }
            in.close();
            return sb.toString();
        }
        return null;
    }
}
