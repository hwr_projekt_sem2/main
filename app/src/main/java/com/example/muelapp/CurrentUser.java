package com.example.muelapp;

public class CurrentUser {
    private static MainUser mainUser = new MainUser(0, null,null,null,null);

    public static void setMainUser(MainUser mainUser) {
        CurrentUser.mainUser = mainUser;
    }

    public static MainUser getMainUser() {
        return mainUser;
    }

    public static int getId() {
        return mainUser.getId();
    }

    public static void setId(int id) {
        mainUser.setId(id);
    }

    public static String getName() {

        return mainUser.getUsername();
    }

    public static void setName(String name) {
        mainUser.setUsername(name);
    }

    public static String getEmail() {
        return mainUser.getEmail();
    }

    public static void setEmail(String email) {
        mainUser.setEmail(email);
    }

    public static String getPassword() {
        return mainUser.getPassword();
    }

    public static void setPassword(String password) {
        mainUser.setPassword(password);
    }

    public static String getToken() {
        return mainUser.getToken();
    }

    public static void setToken(String token) {
        mainUser.setToken(token);
    }
}
