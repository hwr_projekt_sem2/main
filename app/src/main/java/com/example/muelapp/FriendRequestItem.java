package com.example.muelapp;

public class FriendRequestItem {
    private String mText;
    private int userId;

    public FriendRequestItem(String text, int id) {
        mText = text;
        userId = id;
    }

    public String getText() {
        return mText;
    }

    public int getUserId() {
        return userId;
    }
}
