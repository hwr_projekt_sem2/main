package com.example.muelapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class FriendsFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private FriendsAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutmanager;
    private static final String URL_UserSearch = "https://muell.yunamio.de/users/search/";
    private static final String URL_User = "https://muell.yunamio.de/users/";
    private Collection<User> userSearchResult = new ArrayList<>();
    ArrayList<FriendslistItem> friendsList = new ArrayList<>();
    public Collection<Relationship> currentUserRelationships = new ArrayList<>();

    private Button friendReq;
    private EditText findFriends;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friends, container, false);

        findFriends = view.findViewById(R.id.findFriends);
        friendReq = view.findViewById(R.id.friendsReq);
        friendsList.clear();
        currentUserRelationships.clear();
        userSearchResult.clear();

        //Get user relationships
        delayGetCurrentUserRelationships();

        mRecyclerView = view.findViewById(R.id.friendsList);
        mRecyclerView.setHasFixedSize(true);
        mLayoutmanager = new LinearLayoutManager(getContext());
        mAdapter = new FriendsAdapter(friendsList);

        mRecyclerView.setLayoutManager(mLayoutmanager);
        mRecyclerView.setAdapter(mAdapter);

        findFriends.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    String username = findFriends.getText().toString();
                    delaySearchUsers(username);
                    findFriends.getText().clear();
                    handled = true;
                }
                return handled;
            }
        });

        mAdapter.setOnItemClickListener(new FriendsAdapter.OnItemClickListener() {
            @Override
            public void onDeleteClick(final int position) {

                AlertDialog.Builder altdial = new AlertDialog.Builder(getActivity());
                altdial.setMessage("Wollen Sie " + friendsList.get(position).getText1() + " von Ihrer Freundesliste entfernen?").setCancelable(false)
                        //Negativ und Positiv sind für Design-Zwecke vertauscht
                        .setNegativeButton("Ja", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new UnfriendFriendRequestAsync(friendsList.get(position).getFriendsId()).execute();
                                friendsList.remove(position);
                                mAdapter.notifyItemRemoved(position);
                            }
                        })
                        .setPositiveButton("Nein", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = altdial.create();
                alert.setTitle("Entfernen bestätigen");
                alert.show();
            }
        });

        friendReq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(container.getId(), new FriendRequestFragment()).addToBackStack(null).commit();
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.search_friends, menu);
        MenuItem searchItem = menu.findItem(R.id.search_friends);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
        searchView.setQueryHint("Freunde suchen");
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void delaySearchUsers(final String searchQuery) {
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, URL_UserSearch+searchQuery, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Collection<User> userResults = new ArrayList<>();
                            //traversing through all the object
                            for (int i = 0; i < response.length(); i++) {

                                //getting product object from json array
                                JSONObject user = response.getJSONObject(i);

                                //adding the product to product list
                               userResults.add(new User(
                                        user.getInt("id"),
                                        user.getString("username")
                               ));
                            }
                            userSearchResult = userResults;
                            //Fehleranzeige falls Nuzter nicht gefunden wurde
                            if (userSearchResult.isEmpty()) {
                                Toast.makeText(getActivity().getApplicationContext(), "Nutzer nicht gefunden.", Toast.LENGTH_SHORT).show();
                            }
                            for(User resultUser : userSearchResult){
                                //friendsList.add(new FriendslistItem(R.drawable.ic_friends, resultUser.getUsername(), resultUser.getId()));
                                if (resultUser.getUsername().toLowerCase().equals(searchQuery.toLowerCase())) {
                                    new FriendRequestAsync(resultUser.getId()).execute();
                                    Toast.makeText(getActivity().getApplicationContext(), "Anfrage wurde gesendet.", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }

                }){

            @Override
            public Map<String, String> getHeaders(){
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("AUTHENTICATION", CurrentUser.getToken());
                return headers;
            }};
        Volley.newRequestQueue(requireContext()).add(stringRequest);
    }
    private void delayGetCurrentUserRelationships() {
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, URL_User+CurrentUser.getId()+"/relationships", null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Collection<Relationship> relationshipResults = new ArrayList<>();
                            //traversing through all the object
                            for (int i = 0; i < response.length(); i++) {

                                //getting product object from json array
                                JSONObject relationship = response.getJSONObject(i);

                                //adding the product to product list
                                relationshipResults.add(new Relationship(
                                                relationship.getInt("id"),
                                                new User(
                                                relationship.getJSONObject("userActive").getInt("id"),
                                                relationship.getJSONObject("userActive").getString("username")
                                                ),
                                                new User(
                                                relationship.getJSONObject("userPassive").getInt("id"),
                                                relationship.getJSONObject("userPassive").getString("username")
                                                ),
                                                RelationshipType.valueOf(relationship.getString("relationship_type"))
                                        )
                                );
                            }
                            currentUserRelationships = relationshipResults;

                            for (Relationship relationship : currentUserRelationships) {
                                if ((relationship.getRelationship_type() == RelationshipType.INCOMINGFRIEND) && relationship.getActiveUser().getId() == CurrentUser.getId()) {
                                    friendReq.setVisibility(View.VISIBLE);
                                }
                                if ((relationship.getRelationship_type() == RelationshipType.OUTGOINGFRIEND) && relationship.getPassiveUser().getId() == CurrentUser.getId()) {
                                    friendReq.setVisibility(View.VISIBLE);
                                }
                                if(relationship.getRelationship_type() == RelationshipType.FRIEND){
                                    if (relationship.getActiveUser().getId() == CurrentUser.getId()) {
                                        friendsList.add(new FriendslistItem(R.drawable.ic_friends, relationship.getPassiveUser().getUsername(), relationship.getPassiveUser().getId()));
                                        mAdapter.notifyDataSetChanged();
                                    }
                                    if (relationship.getPassiveUser().getId() == CurrentUser.getId()) {
                                        friendsList.add(new FriendslistItem(R.drawable.ic_friends, relationship.getActiveUser().getUsername(), relationship.getActiveUser().getId()));
                                        mAdapter.notifyDataSetChanged();
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }

                }){

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("AUTHENTICATION", CurrentUser.getToken());
                return headers;
            }};
        Volley.newRequestQueue(requireContext()).add(stringRequest);
    }

    private static class FriendRequestAsync extends AsyncTask<String,String,String> {
        private int receivingUserId;
        FriendRequestAsync(int receivingUserId){
            super();
            this.receivingUserId = receivingUserId;
        }
        @Override
        protected String doInBackground(String... strings) {
            try {
                // POST Request
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("type", "FRIEND");
                return RequestHandler.sendPost(URL_User+CurrentUser.getId()+"/relationships/" + receivingUserId +"?type=FRIEND", postDataParams, "post");
            }
            catch(Exception e){
                return "Exception: " + e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String s) {
        }
    }

    private static class UnfriendFriendRequestAsync extends AsyncTask<String,String,String> {
        private int receivingUserId;
        UnfriendFriendRequestAsync(int receivingUserId){
            super();
            this.receivingUserId = receivingUserId;
        }
        @Override
        protected String doInBackground(String... strings) {
            try {
                // POST Request
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("type", "FRIEND");
                return RequestHandler.sendPost(URL_User+CurrentUser.getId()+"/relationships/" + receivingUserId +"?type=UNFRIEND", postDataParams, "post");
            }
            catch(Exception e){
                return "Exception: " + e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String s) {
        }
    }
}
