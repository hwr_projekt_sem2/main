package com.example.muelapp;

public class ActivitylistItem {
    private int activityImage;
    private String activityName;
    private String activityDate;

    public ActivitylistItem(int imageResource, String text1, String text2) {
        activityImage = imageResource;
        activityName = text1;
        activityDate = text2;
    }

    public int getImageResource() {
        return activityImage;
    }

    public String getText1() {
        return activityName;
    }

    public String getText2() {
        return activityDate;
    }

}
