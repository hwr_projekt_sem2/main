package com.example.muelapp;

public class FriendslistItem {
    private int friendsImage;
    private String friendsName;
    private int friendsId;

    public FriendslistItem(int imageResource, String text1, int id) {
        friendsImage = imageResource;
        friendsName = text1;
        friendsId = id;
    }

    public int getImageResource() {
        return friendsImage;
    }

    public String getText1() {
        return friendsName;
    }

    public int getFriendsId() { return friendsId; }

}
