package com.example.muelapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_fragment);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.settings, new SettingsFragment())
                    .commit();
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
    public static class SettingsFragment extends PreferenceFragmentCompat {
       //Öffnen der jeweiligen Hilfefenster
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);

            Preference button = findPreference("about_us");
            button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    open_about_us();
                    return true;
                }
            });
            Preference button2 = findPreference("hilfe");
            button2.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    open_hilfe();
                    return true;
                }
            });

            Preference button3 = findPreference("roadmap");
            button3.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    open_roadmap();
                    return true;
                }
            });

            Preference button4 = findPreference("impressum");
            button4.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    open_impressum();
                    return true;
                }
            });

        }
        public void open_about_us() {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://müll-weg.com/Ueber-uns/"));
            startActivity(intent);
        }
        public void open_hilfe() {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://müll-weg.com/Hilfe/"));
            startActivity(intent);
        }
        public void open_roadmap() {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://müll-weg.com/Roadmap/"));
            startActivity(intent);
        }
        public void open_impressum() {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://müll-weg.com/Impressum/"));
            startActivity(intent);
        }
    }
}
