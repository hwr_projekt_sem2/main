package com.example.muelapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class FriendRequestAdapter extends RecyclerView.Adapter<FriendRequestAdapter.FriendRequestViewHolder> {
    private ArrayList<FriendRequestItem> mFriendRequestList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public String getText(int position) {
        return mFriendRequestList.get(position).getText();
    }

    public static class FriendRequestViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;

        public FriendRequestViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.friend_req_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public FriendRequestAdapter(ArrayList<FriendRequestItem> friendRequestList) {
        mFriendRequestList = friendRequestList;
    }

    @NonNull
    @Override
    public FriendRequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_request_example, parent, false);
        FriendRequestViewHolder evh = new FriendRequestViewHolder(v, mListener);

        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull FriendRequestViewHolder holder, int position) {
        FriendRequestItem currentItem = mFriendRequestList.get(position);

        holder.mTextView.setText(currentItem.getText());
    }

    @Override
    public int getItemCount() {
        return mFriendRequestList.size();
    }
}
