package com.example.muelapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.maps.android.clustering.ClusterManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static androidx.core.content.ContextCompat.checkSelfPermission;

public class HomeFragment extends Fragment implements OnMapReadyCallback {

    private static final String TAG = "HomeFragment";
    private static final float DEFAULT_ZOOM = 17;
    private static final ArrayList<SingleMarker> SINGLE_MARKERS = new ArrayList<SingleMarker>(); //Liste von Marker implementieren, die dann auf der Karte dargestellt werden
    private static final String URL_Litters = "https://muell.yunamio.de/litters";
    //private static final String URL_Litters = "http://10.0.2.2:8080/litters";

    //Link: "http://10.0.2.2:8080/litters" ----> NUR LOKAL GESEHEN, ZU TESTZWECKEN!!!

    private GoogleMap mGoogleMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LocationRequest mLocationRequest;
    private Location mLastLocation;
    private View mView;
    private SingleMarker singleMarker;
    private Marker addedPinAfterClick;
    private boolean wasMullexisted = false;
    private boolean gpsStatus = false;
    private int activatedLocationCallback = 0;
    private ClusterManager <SingleMarker> clusterManager;

    // HomeFragment wird visuell dargestellt
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_home, container, false);
        return mView;
    }

    // MapView wird darauf aufbauend dargestellt
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MapView mapView = mView.findViewById(R.id.google_maps);

        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mFusedLocationProviderClient != null) {
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        }
        mLastLocation = null;
    }

    @SuppressLint("ObsoleteSdkInt")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(requireContext());
        Log.d(TAG, "onMapReady: map is ready");
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        setUpClusterer();
    }


    private void setUpClusterer() {
        /*Hier werden die einzelnen Marker optisch zu einem Pin zusammengefasst, sodass beim Herauszoomen nicht eine Landschaft von
        Markern entsteht.*/
        startAttentionWindow();
        initClusterManager();
        delayReturnEditMarker(); //Darstellen der editierten Marker
        moveCamera(new LatLng(50,11), 5);
        mapLocationProcess();
        addPinOnMapProcess();
        movePinOverMap();
        clickPinToDeleteEditMull();
        showInfoWindowClickPin();
    }

    private void setDesignOfMap() {
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mGoogleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            requireContext(), R.raw.map_style));

            if (!success) {
                Log.e(TAG, "Stilanalyse fehlgeschlagen.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Es wurden kein Design gefunden. Error: ", e);
        }
    }

    private void startAttentionWindow() {
        new AlertDialog.Builder(getContext())
                .setTitle("Achtung!!!")
                .setMessage("Achte beim Nutzen dieser App stets auf deine reale Umgebung!")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Prompt the user once explanation has been shown
                        dialogInterface.cancel();
                    }
                })
                .create()
                .show();
    }

    private void initClusterManager() {
        clusterManager = new ClusterManager<SingleMarker>(requireContext(), mGoogleMap);
        CustomClusterRenderer renderer = new CustomClusterRenderer(requireContext(), mGoogleMap, clusterManager);
        clusterManager.setRenderer(renderer);

        mGoogleMap.setOnCameraIdleListener(clusterManager);
        mGoogleMap.setOnMarkerClickListener(clusterManager);
        mGoogleMap.setOnInfoWindowClickListener(clusterManager);
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(@NonNull LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                checkGPS();
                if (gpsStatus) {
                    //The last location in the list is the newest
                    // Log.i("HomeFragment", "Location: " + location.getLatitude() + " " + location.getLongitude());
                    mLastLocation = locationList.get(locationList.size() - 1);

                    switch (activatedLocationCallback) {
                        case 1:
                            activatedLocationCallback = 0;
                            animateCameraToCurrentLocation();
                            break;
                        case 2:
                            activatedLocationCallback = 0;
                            addPinOnMapAfterButtonClick();
                            break;
                        case 0:
                            break;
                    }
                } else {
                    if (mFusedLocationProviderClient != null) {
                        mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
                       // Log.i(TAG, "LastLocation:" + mLastLocation);
                    }
                    mLastLocation = null;
                    displayTurnGpsOnMessage();
                }
            }
        }
    };

    private void checkGPS() {
        LocationManager locationManager = (LocationManager) requireContext().getSystemService(Context.LOCATION_SERVICE);
        gpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private void checkLocationPermission() {
        if (checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                displayRequestMessage();
            } else {
                // No explanation needed, we can request the permission.
                displayRequestMessage();
            }
        }
    }

    private void displayRequestMessage() {
        new AlertDialog.Builder(requireContext())
                .setTitle("GPS-Ortung wird benötigt!")
                .setMessage("Diese App benötigt die Ortungserlaubnis, bitte akzeptiere die GPS-Ortung, um sie ordnungsgemäß zu verwenden!\n" +
                        "Die GPS-Ortung wird für das Hinzufügen von Müllstellen (Pins) auf der Karte und etc. benötigt.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Prompt the user once explanation has been shown
                        requestPermissions(
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                MY_PERMISSIONS_REQUEST_LOCATION );
                    }
                })
                .create()
                .show();
    }

    private void displayTurnGpsOnMessage() {
        new AlertDialog.Builder(requireContext())
                .setTitle("GPS-Ortung wird benötigt!")
                .setMessage("Die GPS-Ortung wird für das Hinzufügen von Müllstellen (Pins) auf der Karte und etc. benötigt.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Prompt the user once explanation has been shown
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                })
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (checkSelfPermission(requireContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Objects.requireNonNull(Looper.myLooper()));
                        mGoogleMap.setMyLocationEnabled(true);
                        checkClickOnGPSButton();
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(requireContext(), "Zugang nicht erlaubt!", Toast.LENGTH_LONG).show();
                }
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void mapLocationProcess() {
        //Dieser Client bietet Funktionen bezüglich der GPS-Ortung an
        //Das besondere an ihm ist, dass er dafür sorgt, dass der aktuelle Standort alle 10 Sekunden aktualisiert wird
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireContext());

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(1000); // two minute interval = 120000 ---> momentane Einstellung: 5 Sekunden
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (checkSelfPermission(requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Objects.requireNonNull(Looper.myLooper()));
                mGoogleMap.setMyLocationEnabled(true);
                checkClickOnGPSButton();
        } else {
            //Request Location Permission
            checkLocationPermission();
        }
    }

    private void checkClickOnGPSButton() {
        mGoogleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public boolean onMyLocationButtonClick() {
                checkGPS();
                activatedLocationCallback = 0;
                if (!gpsStatus) {
                    mLastLocation = null;
                    displayTurnGpsOnMessage();
                } else {
                    if (mLastLocation != null) {
                        animateCameraToCurrentLocation();
                    } else {
                        activatedLocationCallback = 1;
                        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Objects.requireNonNull(Looper.myLooper()));
                    }
                }
                return true;
            }
        });
    }

    private void animateCameraToCurrentLocation() {
        LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
    }

    private void addPinOnMapProcess() {
        FloatingActionButton buttonMap = mView.findViewById(R.id.button_map);
        buttonMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkGPS();
                activatedLocationCallback = 0;

                if (checkSelfPermission(requireContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(requireContext(), "Die Berechtigung für die GPS-Ortung muss erteilt werden!\nLaden Sie \"Home\" neu!", Toast.LENGTH_LONG).show();
                } else {
                    if (gpsStatus) {
                        if (mLastLocation != null) {
                            addPinOnMapAfterButtonClick();
                        } else {
                            activatedLocationCallback = 2;
                            mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Objects.requireNonNull(Looper.myLooper()));
                        }
                    } else {
                        mLastLocation = null;
                        displayTurnGpsOnMessage();
                    }
                }
            }
        });
    }

    private void addPinOnMapAfterButtonClick() {
        LatLng latLng = new LatLng(mLastLocation.getLatitude() + 0.0001, mLastLocation.getLongitude() + 0.001);
        DecimalFormat df = new DecimalFormat("#.####");

        if (addedPinAfterClick != null) {
            addedPinAfterClick.remove();
        }
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.draggable(true);
        markerOptions.position(latLng);
        markerOptions.title("Position: " + df.format(latLng.latitude) + ", " + df.format(latLng.longitude));
        addedPinAfterClick = mGoogleMap.addMarker(markerOptions);
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
    }

    private void movePinOverMap() {
        mGoogleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(@NonNull Marker marker) {
                onMarkerDragPositionDelay(marker);
            }

            @Override
            public void onMarkerDrag(@NonNull Marker marker) {
                onMarkerDragPositionDelay(marker);
            }

            @Override
            public void onMarkerDragEnd(@NonNull Marker marker) {
                DecimalFormat df = new DecimalFormat("#.####");
                LatLng position = marker.getPosition();
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(position);
                markerOptions.title("Position: " + df.format(position.latitude) + ", " + df.format(position.longitude));
                openEditWindowMull(marker);
            }
        });
    }

    void clickPinToDeleteEditMull() {
        mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @SuppressLint("SimpleDateFormat")
            @Override
            public void onInfoWindowClick(final Marker marker) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(requireContext(), R.style.AlertDialogCustom);
                builder.setTitle("Selektierte Müllstelle bearbeiten oder löschen?")
                        .setNegativeButton("Müllstelle löschen!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final AlertDialog.Builder builderNachfrage = new AlertDialog.Builder(requireContext(), R.style.AlertDialogCustom);
                        builderNachfrage.setMessage("Möchtest du wirklich die ausgewählte Müllstelle löschen?").setPositiveButton("Müllstelle löschen!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                compareMarkerWithSingleMarkersToDelete(marker);
                            }
                        }).setCancelable(true).setNegativeButton("Abbruch!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        final AlertDialog alertNachfrage = builderNachfrage.create();
                        alertNachfrage.show();
                    }
                }).setPositiveButton("Müllstelle bearbeiten!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openEditWindowMull(marker);
                    }
                });
                final AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    private void showInfoWindowClickPin() {
        //Hier kann man das Layout von den Infofenstern der einzelnen Marker festlegen
        mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                @SuppressLint("InflateParams") View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);
                if (marker.getTitle() != null) {
                    TextView infoWindow = v.findViewById(R.id.text_view_info);
                    infoWindow.setText(marker.getTitle());

                    mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
                } else {
                    mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
                    v = null;
                }
                return v;
            }
        });
    }

    private void compareMarkerWithSingleMarkersToDelete(Marker marker) {
        // IF-Abfrage: Marker aus Liste mit Google Marker vergleichen, wenn SingleMarker übereinstimmt, dann getId()
        for (int i = 0; i < SINGLE_MARKERS.size(); i++) {
            SingleMarker wishMarker = SINGLE_MARKERS.get(i);
            if (wishMarker.getLatitude() == marker.getPosition().latitude
                    && wishMarker.getLongitude() == marker.getPosition().longitude) {
                singleMarker = wishMarker;
                deleteSingleMarker(singleMarker);
                clusterManager.removeItem(singleMarker);
                marker.remove();
            }
        }
        clusterManager.cluster();
    }

    private void onMarkerDragPositionDelay(@NonNull Marker marker) {
        DecimalFormat df = new DecimalFormat("#.####");
        LatLng position = marker.getPosition();
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(position.latitude, position.longitude), DEFAULT_ZOOM));
        marker.setTitle("Position: " + df.format(position.latitude) + ", " + df.format(position.longitude));
        marker.showInfoWindow();
    }

    //Kann zum Navigieren bezüglich der Position von Markern (Müllstellen) verwendet werden
    private void moveCamera(@NonNull LatLng latLng, float zoom) {
        Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,zoom));
    }

    //Hier wurde die Android Volley Library verwendet, um Anfragen zum Server schicken zu können
    private void delayReturnEditMarker() {
        mGoogleMap.clear();
        clusterManager.clearItems();
        SINGLE_MARKERS.clear();
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, URL_Litters, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            //traversing through all the object
                            for (int i = 0; i < response.length(); i++) {

                                //getting product object from json array
                                JSONObject marker = response.getJSONObject(i);

                                //adding the product to product list
                                SINGLE_MARKERS.add(new SingleMarker(
                                        marker.getInt("id"),
                                        marker.getString("title"),
                                        marker.getString("description"),
                                        marker.getDouble("latitude"),
                                        marker.getDouble("longitude")
                                ));
                            }
                            addMarkerOnMap();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }

                }){

        @NonNull
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            HashMap<String, String> headers = new HashMap<String, String>();
            headers.put("Accept", "application/json");
            headers.put("AUTHENTICATION", CurrentUser.getToken());
            return headers;
        }};
        Volley.newRequestQueue(requireContext()).add(stringRequest);
    }

    private void addMarkerOnMap() {
        for (SingleMarker singleMarkerNeu : SINGLE_MARKERS) {
            String title = "Bezeichnung: " + singleMarkerNeu.getTitle() + "\n" + "\n" + "Beschreibung: " + "\n" + singleMarkerNeu.getDescription();
            singleMarkerNeu.setTitle(title);

            clusterManager.addItem(singleMarkerNeu);
            clusterManager.cluster();
        }
    }

    private void deleteSingleMarker(@NonNull final SingleMarker singleMarker) {
        String deleteUrl = URL_Litters + "/" + singleMarker.getId();
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, deleteUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Toast.makeText(requireContext(), response, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error.
                    }
                }){

            @NonNull
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Accept", "application/json");
                headers.put("AUTHENTICATION", CurrentUser.getToken());
                return headers;
            }};;
        Volley.newRequestQueue(requireContext()).add(stringRequest);
    }

    private void openEditWindowMull(@NonNull Marker marker) {
        if (SINGLE_MARKERS.size() != 0) {
            for (int i = 0; i < SINGLE_MARKERS.size(); i++) {
                SingleMarker wishMarker = SINGLE_MARKERS.get(i);
                String wishMarkerTitle = wishMarker.getTitle();

                if (wishMarkerTitle.equals(marker.getTitle()) && wishMarker.getLatitude() == marker.getPosition().latitude
                        && wishMarker.getLongitude() == marker.getPosition().longitude) {
                    singleMarker = wishMarker;
                    wasMullexisted = true;
                    break;
                } else {
                    singleMarker = new SingleMarker(0, "", "", marker.getPosition().latitude, marker.getPosition().longitude);
                }
            }
        } else {
            Toast.makeText(requireContext(), "Ein Fehler ist aufgetreten!", Toast.LENGTH_LONG).show();
        }
        Intent intent = new Intent(requireContext(), MapInfoClickActivity.class);
        intent.putExtra("selectedMarkerPosition", singleMarker).
                putExtra("wasMarkerExisted", wasMullexisted);
        startActivity(intent);
    }
}
