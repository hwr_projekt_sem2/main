package com.example.muelapp;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

public class GetDisplayMobileSize {
    private int width, height;
    private final Context mContext;

    public GetDisplayMobileSize(Context mContext) {
        this.mContext = mContext;
        getCurrentDisplaySize();
    }

    private void getCurrentDisplaySize() {
        final WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
}
