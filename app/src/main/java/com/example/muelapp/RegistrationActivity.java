package com.example.muelapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Objects;

public class RegistrationActivity extends AppCompatActivity {

    private static final String URL_USERS="https://muell.yunamio.de/users";

    private EditText inputUsername;
    private EditText inputEmail;
    private EditText inputPassword;
    private Button showPasswordButton;
    private Drawable showPasswordEye;
    private Drawable hidePasswordEye;
    private int countSubmitShowPassword;
    private MainUser mainUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        GetDisplayMobileSize getDisplayMobileSize = new GetDisplayMobileSize(getApplicationContext());
        int width = getDisplayMobileSize.getWidth();
        int height = getDisplayMobileSize.getHeight();

        if (width < 800 && height < 800) {
            setContentView(R.layout.registration_activity_small);
        } else {
            setContentView(R.layout.registration_activity);
        }

        inputPassword = findViewById(R.id.editTextPasswordRegistration);

        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.registration_toolbar);
        showPassword();
        registrateAccount();
        tryLogIn();
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void showPassword() {
        showPasswordButton = findViewById(R.id.showPasswordButtonRegistration);
        showPasswordEye = getDrawable(R.drawable.ic_baseline_show_password_eye_24);
        hidePasswordEye = getDrawable(R.drawable.ic_baseline_visibility_off_password_24);

        showPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countSubmitShowPassword++;
                switch(countSubmitShowPassword) {
                    case 1:  showPasswordButton.setBackground(hidePasswordEye);
                        break;
                    case 4: countSubmitShowPassword = 0;
                        break;
                }

                if(showPasswordButton.getBackground().getCurrent().equals(hidePasswordEye)) {
                    inputPassword.setTransformationMethod(null);
                    showPasswordButton.setBackground(showPasswordEye);
                } else {
                    inputPassword.setTransformationMethod(new PasswordTransformationMethod());
                    showPasswordButton.setBackground(hidePasswordEye);
                }
            }
        });
    }

    private void registrateAccount() {
        Button registrateButton = findViewById(R.id.registrate_button);
        registrateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrateAccountProcess();
            }
        });

        inputPassword.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    registrateAccountProcess();
                    return true;
                }
                return false;
            }
        });
    }

    private void registrateAccountProcess() {
        boolean usernameCorrect = validateUsername();
        boolean emailCorrect = validateEmail();
        boolean passwordCorrect = validatePassword();
        if(usernameCorrect && emailCorrect && passwordCorrect) {
            mainUser = new MainUser(0, inputUsername.getText().toString().trim(), inputEmail.getText().toString().trim(), inputPassword.getText().toString().trim(), null);
            try {
                String jsonToken = new RequestAsync().execute().get();
                //Deserialize JSON
                Gson g = new Gson();
                MainUser tokenMainUser = g.fromJson(jsonToken, MainUser.class);
                CurrentUser.setMainUser(tokenMainUser);
            }catch (Exception e){
            }
            Toast.makeText(getApplicationContext(), "Registrierung war erfolgreich", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(), GoogleMapActivity.class));
            this.finish();
        }
    }

    private void tryLogIn() {
        Button tryLogInButton = findViewById(R.id.login_retry_button);
        tryLogInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });
    }

    private boolean validateUsername() {
        inputUsername = findViewById(R.id.editTextUsernameRegistration);
        String inputUsernameText = inputUsername.getText().toString().trim();

        if (inputUsernameText.isEmpty()) {
            inputUsername.setError("Feld darf nicht leer sein!");
            return false;
        } else {
            inputUsername.setError(null);
            return true;
        }
    }

    private boolean validateEmail() {
        inputEmail = findViewById(R.id.editTextEmailAddressRegistration);
        String emailInput = inputEmail.getText().toString().trim();

        if (emailInput.isEmpty()) {
            inputEmail.setError("Feld darf nicht leer sein!");
            return false;
        } else if (!emailInput.contains("@")) {
            inputEmail.setError("Keine richtige Email angegeben!");
            return false;
        } else
            inputEmail.setError(null);
        return true;
    }

    private boolean validatePassword() {
        TextInputLayout layoutPasswordError = findViewById(R.id.layoutPasswordRegistration);
        String textPassword = inputPassword.getText().toString().trim();

        if (textPassword.isEmpty()) {
            layoutPasswordError.setError("Feld darf nicht leer sein!");
            return false;
        } else if (textPassword.length() > 20) {
            layoutPasswordError.setError("Passwort ist zu lang!");
            return false;
        } else {
            layoutPasswordError.setError(null);
            layoutPasswordError.setErrorEnabled(false);
            return true;
        }
    }

    public void hideKeyboard(@NonNull View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private class RequestAsync extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                // POST Request
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("email", mainUser.getEmail());
                postDataParams.put("username", mainUser.getUsername());
                postDataParams.put("password", mainUser.getPassword());
                postDataParams.put("verified", false);
                return RequestHandler.sendPost(URL_USERS, postDataParams, "post");
            }
            catch(Exception e){
                return "Exception: " + e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if(s!=null){
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed () {}
}
