package com.example.muelapp;

public class Relationship {

    private int id;

    private User activeUser;

    private User passiveUser;

    private RelationshipType relationship_type;

    public Relationship(int id, User activeUser, User passiveUser, RelationshipType relationship_type) {
        this.id = id;
        this.activeUser = activeUser;
        this.passiveUser = passiveUser;
        this.relationship_type = relationship_type;
    }

    public int getId() {
        return id;
    }

    public User getActiveUser() {
        return activeUser;
    }

    public User getPassiveUser() {
        return passiveUser;
    }

    public RelationshipType getRelationship_type() {
        return relationship_type;
    }

    public void setRelationship_type(RelationshipType relationship_type) {
        this.relationship_type = relationship_type;
    }
}