package com.example.muelapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    private static final String URL_LOGIN="https://muell.yunamio.de/login";
    private EditText inputEmail;
    private EditText inputPassword;
    private Button showPasswordButton;
    private Drawable showPasswordEye;
    private Drawable hidePasswordEye;
    private MainUser mainUser;
    private int countSubmitShowPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        GetDisplayMobileSize getDisplayMobileSize = new GetDisplayMobileSize(getApplicationContext());
        int width = getDisplayMobileSize.getWidth();
        int height = getDisplayMobileSize.getHeight();

        if (width < 800 && height < 800) {
            setContentView(R.layout.activity_main_small);
        } else {
            setContentView(R.layout.activity_main);
        }

        inputPassword = findViewById(R.id.editTextPasswort);

        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.login_toolbar);
        showPassword();
        logIn();
        registerAccount();
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void showPassword() {
        showPasswordButton = findViewById(R.id.showPasswordButtonLogIn);
        showPasswordEye = getDrawable(R.drawable.ic_baseline_show_password_eye_24);
        hidePasswordEye = getDrawable(R.drawable.ic_baseline_visibility_off_password_24);

        showPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countSubmitShowPassword++;
                switch(countSubmitShowPassword) {
                    case 1:  showPasswordButton.setBackground(hidePasswordEye);
                        break;
                    case 4: countSubmitShowPassword = 0;
                        break;
                }

                if(showPasswordButton.getBackground().getCurrent().equals(hidePasswordEye)) {
                    inputPassword.setTransformationMethod(null);
                    showPasswordButton.setBackground(showPasswordEye);
                } else {
                    inputPassword.setTransformationMethod(new PasswordTransformationMethod());
                    showPasswordButton.setBackground(hidePasswordEye);
                }
            }
        });
    }

    private void logIn() {
        Button submitLogInButton = findViewById(R.id.login_button);
        submitLogInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logInProcess();
            }
        });

        inputPassword.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    logInProcess();
                    return true;
                }
                return false;
            }
        });
    }

    private void logInProcess() {
        boolean emailCorrect = validateEmail();
        boolean passwordCorrect = validatePassword();
        mainUser = new MainUser(0,null,inputEmail.getText().toString().trim(),inputPassword.getText().toString().trim(), null );
        if(emailCorrect && passwordCorrect) {
            try {
                String jsonToken = new RequestAsync().execute().get();
                //Deserialize JSON
                if(jsonToken != null) {
                    Gson g = new Gson();
                    MainUser tokenMainUser = g.fromJson(jsonToken, MainUser.class);
                    CurrentUser.setMainUser(tokenMainUser);
                    Toast.makeText(getApplicationContext(), "Anmelden war erfolgreich", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), GoogleMapActivity.class));
                    this.finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Anmelden Fehlgeschlagen", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                }
            } catch (Exception e) {
            }
        }
    }

    private void registerAccount() {
        Button submitRegisterAccount = findViewById(R.id.createAccount_button);
        submitRegisterAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), RegistrationActivity.class));
            }
        });
    }

    private boolean validateEmail() {
        inputEmail = findViewById(R.id.editTextEmailAddress);
        String emailInput = inputEmail.getText().toString().trim();

        if (emailInput.isEmpty()) {
            inputEmail.setError("Feld darf nicht leer sein!");
            return false;
        } else {
            inputEmail.setError(null);
            return true;
        }
    }

    private boolean validatePassword() {
        TextInputLayout layoutPasswordError = findViewById(R.id.layoutPassword);
        String textPassword = inputPassword.getText().toString().trim();

        if (textPassword.isEmpty()) {
            layoutPasswordError.setError("Feld darf nicht leer sein!");
            return false;
        } else if (textPassword.length() > 20) {
            layoutPasswordError.setError("Passwort ist zu lang!");
            return false;
        } else {
            layoutPasswordError.setError(null);
            layoutPasswordError.setErrorEnabled(false);
            return true;
        }
    }

    public void hideKeyboard(@NonNull View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private class RequestAsync extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                // POST Request
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("username", mainUser.getEmail());
                postDataParams.put("password", mainUser.getPassword());
                return RequestHandler.sendPost(URL_LOGIN, postDataParams, "post");
            }
            catch(Exception e){
                return "Exception: " + e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String s) {
        }
    }

    @Override
    public void onBackPressed () {}
}