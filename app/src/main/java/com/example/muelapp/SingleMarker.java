package com.example.muelapp;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class SingleMarker implements Parcelable, ClusterItem {

    //Marker-Objekt namens SingleMarker, um Parameter über neue Activities mitgeben zu können und für weitere Methoden....
    private int id;
    private String title;
    private String description;
    private double latitude;
    private double longitude;
    private LatLng position;

    public SingleMarker(int id, String title, String description, double latitude, double longitude) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
        position = new LatLng(latitude, longitude);
    }

    protected SingleMarker(@NonNull Parcel in) {
        id = in.readInt();
        title = in.readString();
        description = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    public static final Creator<SingleMarker> CREATOR = new Creator<SingleMarker>() {
        @Override
        public SingleMarker createFromParcel(Parcel in) {
            return new SingleMarker(in);
        }

        @Override
        public SingleMarker[] newArray(int size) {
            return new SingleMarker[size];
        }
    };

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    @Override
    public LatLng getPosition() {
        return position;
    }

    //nicht verwenden!
    @Override
    public String getSnippet() {
        return null;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }
}
