package com.example.muelapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONObject;

import java.util.Objects;

public class MapInfoClickActivity extends AppCompatActivity {

    private static final String URL_Litters = "https://muell.yunamio.de/litters";
    //private static final String URL_Litters = "http://10.0.2.2:8080/litters";
    //Link: "http://10.0.2.2:8080/litters" ----> NUR LOKAL GESEHEN, ZU TESTZWECKEN!!!

    private TextInputEditText editTextBezeichnung;
    private TextInputEditText editTextBeschreibung;
    private String editBezeichnung;
    private String editBeschreibung;
    private boolean wasMullExisted;
    private SingleMarker singleMarker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        GetDisplayMobileSize getDisplayMobileSize = new GetDisplayMobileSize(getApplicationContext());
        int width = getDisplayMobileSize.getWidth();
        int height = getDisplayMobileSize.getHeight();

        if (width < 800 && height < 800) {
            setContentView(R.layout.activity_map_info_click_small);
        } else {
            setContentView(R.layout.activity_map_info_click);
        }

        editTextBezeichnung = findViewById(R.id.editMüllstelleName);
        editTextBeschreibung = findViewById(R.id.editMüllstelleBeschreibung);
        FloatingActionButton submitButton = findViewById(R.id.editSubmitButton);

        Intent intent = getIntent();
        singleMarker = intent.getParcelableExtra("selectedMarkerPosition");
        wasMullExisted = intent.getBooleanExtra("wasMarkerExisted", false);
        if (wasMullExisted) {
            String title = singleMarker.getTitle();
            title = removeWord(title, "Bezeichnung:");
            title = title.split("\n")[0];

            editTextBezeichnung.setText(title);
            editTextBeschreibung.setText(singleMarker.getDescription());
        }

        submitButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                editBezeichnung = Objects.requireNonNull(editTextBezeichnung.getText()).toString();
                editBeschreibung = Objects.requireNonNull(editTextBeschreibung.getText()).toString();

                if (countOccurrences(editBeschreibung, '\n') > 11) {
                    createToastWarning("WARNUNG!!!" + "\n" + "Es dürfen nicht mehr als 11 Zeilen zum Schreiben verwendet werden!" + "\n" + "Sie müssen dies korregieren, erst dann wird die Müllstelle gespeichert!");
                    editTextBeschreibung.setFilters(new InputFilter[]{new MaxLinesInputFilter(11)});

                } else if (countDigits(editBeschreibung) > 300) {
                    createToastWarning("WARNUNG!!!" + "\n" + "Es dürfen nicht mehr als 300 Zeichen geschrieben werden!" + "\n" + "Sie müssen dies korregieren, erst dann wird die Müllstelle gespeichert!");

                } else if ((editBezeichnung.equals("") || editBeschreibung.equals(""))) {
                    createToastWarning("WARNUNG!!!" + "\n" + "Es wurde nichts in der Bezeichnung oder in der Beschreibung der Müllstelle eingegeben!" + "\n" + "Sie müssen bei beiden Feldern Angaben machen, erst dann wird die Müllstelle gespeichert!");

                } else {
                    Objects.requireNonNull(singleMarker).setTitle(editBezeichnung);
                    Objects.requireNonNull(singleMarker).setDescription(editBeschreibung);

                    if(wasMullExisted) {
                        //UPDATE
                        new RequestAsync2().execute();
                    } else {
                        //POST
                        new RequestAsync().execute();
                    }
                    startActivity(new Intent(getApplicationContext(), GoogleMapActivity.class));
                    MapInfoClickActivity.this.finish();
                }
            }
        });
    }

    private int countOccurrences(@NonNull String string, char charAppearance) {
        int count = 0;
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == charAppearance) {
                count++;
            }
        }
        return count;
    }

    private int countDigits(@NonNull String string) {
        int count = 0;
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) != ' ') {
                count++;
            }
        }
        return count;
    }

    @NonNull
    private String removeWord(@NonNull String string, String word)
    {
        // Check if the word is present in string
        // If found, remove it using removeAll()
        if (string.contains(word)) {
            String tempWord = word + " ";
            string = string.replaceAll(tempWord, "");
            tempWord = " " + word;
            string = string.replaceAll(tempWord, "");
        }
        // Return the resultant string
        return string;
    }

    public void createToastWarning(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.warning_toast, (ViewGroup) findViewById(R.id.warning_toast_container));
        for (int i = 0; i < 3; i++) {
            TextView text = layout.findViewById(R.id.text);
            text.setText(message);
            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.DISPLAY_CLIP_HORIZONTAL, 0, 0);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();
        }
    }

    public void hideKeyboard(@NonNull View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private class RequestAsync extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                // POST Request
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("title", singleMarker.getTitle());
                postDataParams.put("description", singleMarker.getDescription());
                postDataParams.put("radiant", 0);
                postDataParams.put("resolved", false);
                postDataParams.put("latitude", singleMarker.getLatitude());
                postDataParams.put("longitude", singleMarker.getLongitude());
                return RequestHandler.sendPost(URL_Litters, postDataParams, "post");
            }
            catch(Exception e){
                return "Exception: " + e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if(s!=null){
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
            }
        }
    }

     private class RequestAsync2 extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {

            try {
                final JSONObject postParam = new JSONObject();
                postParam.put("id", singleMarker.getId());
                postParam.put("title", singleMarker.getTitle());
                postParam.put("description", singleMarker.getDescription());
                postParam.put("radiant", 0);
                postParam.put("resolved", false);
                postParam.put("latitude", singleMarker.getLatitude());
                postParam.put("longitude", singleMarker.getLongitude());

                return RequestHandler.sendPost(URL_Litters + "/" + singleMarker.getId(), postParam, "patch");
            } catch (Exception e) {
                return "Exception: " + e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if (s != null) {
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
            }
        }
    }
}

