package com.example.muelapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class ProfileHomeFragment extends Fragment {

    private TextView textProfilName;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profilefragment_home, container, false);

        textProfilName = view.findViewById(R.id.textProfileName);
        textProfilName.setText(CurrentUser.getName());

        return view;
    }
}
