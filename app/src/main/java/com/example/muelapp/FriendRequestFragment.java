package com.example.muelapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class FriendRequestFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private FriendRequestAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private static final String URL_UserSearch = "https://muell.yunamio.de/users/search/";
    private Collection<User> userSearchResult = new ArrayList<>();
    private static final String URL_User = "https://muell.yunamio.de/users/";
    private Collection<Relationship> currentUserRelationships = new ArrayList<>();

    ArrayList<FriendRequestItem> friendRequestList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.friend_request, container, false);

        delayGetCurrentUserRelationships();

        mRecyclerView = view.findViewById(R.id.friend_requests);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mAdapter = new FriendRequestAdapter(friendRequestList);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new FriendRequestAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final int position) {
                AlertDialog.Builder altdial = new AlertDialog.Builder(getActivity());
                altdial.setMessage("Wollen Sie " + mAdapter.getText(position) + " als Freund hinzufügen?").setCancelable(false)
                        //Negativ und Positiv sind für Design-Zwecke vertauscht
                        .setNegativeButton("Ja", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                delaySearchUsers(mAdapter.getText(position));
                                friendRequestList.remove(position);
                                mAdapter.notifyDataSetChanged();
                                if (friendRequestList.isEmpty()) {
                                    getActivity().getSupportFragmentManager().popBackStackImmediate();
                                }
                            }
                        })
                        .setPositiveButton("Nein", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new UnfriendFriendRequestAsync(friendRequestList.get(position).getUserId()).execute();
                                friendRequestList.remove(position);
                                mAdapter.notifyItemRemoved(position);
                                if (friendRequestList.isEmpty()) {
                                    getActivity().getSupportFragmentManager().popBackStackImmediate();
                                }
                            }
                        });
                AlertDialog alert = altdial.create();
                alert.setTitle("Hinzufügen bestätigen");
                alert.show();
            }
        });

        return view;
    }

    private void delayGetCurrentUserRelationships() {
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, URL_User+CurrentUser.getId()+"/relationships", null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Collection<Relationship> relationshipResults = new ArrayList<>();
                            //traversing through all the object
                            for (int i = 0; i < response.length(); i++) {

                                //getting product object from json array
                                JSONObject relationship = response.getJSONObject(i);

                                //adding the product to product list
                                relationshipResults.add(new Relationship(
                                                relationship.getInt("id"),
                                                new User(
                                                        relationship.getJSONObject("userActive").getInt("id"),
                                                        relationship.getJSONObject("userActive").getString("username")
                                                ),
                                                new User(
                                                        relationship.getJSONObject("userPassive").getInt("id"),
                                                        relationship.getJSONObject("userPassive").getString("username")
                                                ),
                                                RelationshipType.valueOf(relationship.getString("relationship_type"))
                                        )
                                );
                            }
                            currentUserRelationships.clear();
                            currentUserRelationships = relationshipResults;

                            for (Relationship relationship : currentUserRelationships) {
                                if ((relationship.getRelationship_type() == RelationshipType.INCOMINGFRIEND) && relationship.getActiveUser().getId() == CurrentUser.getId()) {
                                    friendRequestList.add(new FriendRequestItem(relationship.getPassiveUser().getUsername(), relationship.getPassiveUser().getId()));
                                    mAdapter.notifyDataSetChanged();
                                }
                                if ((relationship.getRelationship_type() == RelationshipType.OUTGOINGFRIEND) && relationship.getPassiveUser().getId() == CurrentUser.getId()) {
                                    friendRequestList.add(new FriendRequestItem(relationship.getActiveUser().getUsername(), relationship.getActiveUser().getId()));
                                    mAdapter.notifyDataSetChanged();
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }

                }){

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("AUTHENTICATION", CurrentUser.getToken());
                return headers;
            }};
        Volley.newRequestQueue(requireContext()).add(stringRequest);
    }

    private void delaySearchUsers(final String searchQuery) {
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, URL_UserSearch+searchQuery, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Collection<User> userResults = new ArrayList<>();
                            //traversing through all the object
                            for (int i = 0; i < response.length(); i++) {

                                //getting product object from json array
                                JSONObject user = response.getJSONObject(i);

                                //adding the product to product list
                                userResults.add(new User(
                                                user.getInt("id"),
                                                user.getString("username")
                                        )
                                );
                            }
                            userSearchResult.clear();
                            //friendsList.clear();
                            userSearchResult = userResults;

                            for(User resultUser : userSearchResult){
                                new FriendRequestAsync(resultUser.getId()).execute();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }

                }){

            @Override
            public Map<String, String> getHeaders(){
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("AUTHENTICATION", CurrentUser.getToken());
                return headers;
            }};
        Volley.newRequestQueue(requireContext()).add(stringRequest);
    }

    private class FriendRequestAsync extends AsyncTask<String,String,String> {
        private int receivingUserId;
        FriendRequestAsync(int receivingUserId){
            super();
            this.receivingUserId = receivingUserId;
        }
        @Override
        protected String doInBackground(String... strings) {
            try {
                // POST Request
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("type", "FRIEND");
                return RequestHandler.sendPost(URL_User+CurrentUser.getId()+"/relationships/" + receivingUserId +"?type=FRIEND", postDataParams, "post");
            }
            catch(Exception e){
                return "Exception: " + e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String s) {
        }
    }

    private static class UnfriendFriendRequestAsync extends AsyncTask<String,String,String> {
        private int receivingUserId;
        UnfriendFriendRequestAsync(int receivingUserId){
            super();
            this.receivingUserId = receivingUserId;
        }
        @Override
        protected String doInBackground(String... strings) {
            try {
                // POST Request
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("type", "FRIEND");
                return RequestHandler.sendPost(URL_User+CurrentUser.getId()+"/relationships/" + receivingUserId +"?type=UNFRIEND", postDataParams, "post");
            }
            catch(Exception e){
                return "Exception: " + e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String s) {
        }
    }
}
