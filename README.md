# Müll Weg - Frontend

Dies ist das Repository vom Front-End.

## Struktur

- [Repository](https://gitlab.com/hwr_projekt_sem2/main/-/tree/master)
- [Issues](https://gitlab.com/hwr_projekt_sem2/main/-/issues)
- [Wiki](https://gitlab.com/hwr_projekt_sem2/main/-/wikis/home)

## Queerverweis Backend

Der Quellcode für die API, der Aufbau der API und der Aufbau der Datenbank befinden sich im [Backend](https://gitlab.com/hwr_projekt_sem2/backend).

- Repository des Backend: https://gitlab.com/hwr_projekt_sem2/backend/-/tree/master
- Issues des Backend: https://gitlab.com/hwr_projekt_sem2/backend/-/issues
- Wiki des Backend: https://gitlab.com/hwr_projekt_sem2/backend/-/wikis/home

## Installationsanleitung

Die Installationsanleitung für das Backend kann im [Wiki](https://gitlab.com/hwr_projekt_sem2/main/-/wikis/Installationsanleitung) gefunden werden.
